# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2021/11/5 13:22
@describe: 
"""
from db_operate.conn_db import ConnMysql
from common.excep import excep
from log.ulog import Ulog
from common.common import Common

import allure

log = Ulog().logger_()


class SDI(ConnMysql):

	@excep
	@allure.step("获取package对应的item")
	def get_items(self, env: str, **kwargs):
		"""获取package对应的item"""
		sql = f"""
		select title,description from item where
		id in(select item_id from item_package_map where
		item_package_id={kwargs["package_id"]});
		"""
		res = self.exec_select(sql, env, "sdi")
		return res