# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-10-14 10:01:00
@describe: 异常处理装饰器
"""

from log.ulog import Ulog
from datetime import datetime
import traceback

log = Ulog().logger_()

def excep(func):
	try:
		return func
	except Exception:
		sign = '=' * 60 + '\n'
		log.error(f"""
		\n{sign}>>>异常时间: \t{datetime.now()}\n>>>异常函数: \t{func.__name__}\n
		>>>异常堆栈: \t{traceback.format_exc()}{sign}\n
		""")
