# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 
"""

import allure
import pytest


def pytest_addoption(parser):
	parser.addoption("--env", default="test", choices=["qa", "test", "prod"], help="运行环境")


@pytest.fixture(scope="session")
@allure.step("接收外部参数判断运行环境")
def env(pytestconfig):
	return pytestconfig.getoption("--env")
