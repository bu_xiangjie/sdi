# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2021/11/8 16:51
@describe: 
"""

from common.open_excel import excel_table_byname
from common.common import Common
from db_operate.saas_operate import Saas
from db_operate.sdi_operate import SDI
from assertpy import soft_assertions
from assertpy import assert_that
from log.ulog import Ulog

import allure
import sys
import os
import json
import pytest


@allure.feature("准实时指标")
class TestSdi:
	log = Ulog().logger_()
	file = "/data/test_sdi.xlsx"
	data = excel_table_byname(file, "sdi")
	ids = [f'{d["casename"]}' for d in data]
	datas = zip(data, ids)

	@allure.title("-")
	@allure.severity(allure.severity_level.BLOCKER)
	@pytest.mark.parametrize("data,ids", datas)
	def test_sdi(self, data, ids, env):
		self.log.info(f"casename:{ids}")
		params = json.loads(data["param"])
		need_info = data["need_info"].split(",")
		use_source = data["use_source"]

		# 组装所有参数
		kwg = {
			"package_id": params["package_id"],
			"product_code": params["request_param"].get("productCode", None),
			"source_code": params["request_param"].get("sourceCode", None),
			"user_group": params["request_param"].get("userGroupProps", None),
			"scene_holder_id": None,
			"enterprise": None,
			"relation_ship": None,
			"channel": params["request_param"].get("channel", None),
			"term": None,
			"is_doctor": None,
			"phone": params["request_param"].get("contactDetail.phone", None),
			"id": params["request_param"].get("IDCardInfo.cardNum", None),
			"vin": None,
			"legal_id": params["request_param"].get("WorkCompanyLegalPerson.cardNum", None),
			"legal_phone": params["request_param"].get("WorkCompanyLegalPerson.mobile", None),
			"work_company_id": params["request_param"].get("WorkCompanyLegalPerson.workCompanyId", None),
			"busi_type": data["busi_type"],
			"product": data["product"]
		}

		# 获取数据库中最新数据,重新组装请求参数
		if "person" in need_info:
			if use_source == "是":
				kwg["id"] = Common.info_crypt(env, params["request_param"]["IDCardInfo.cardNum"], True)
				kwg["phone"] = Common.info_crypt(env, params["request_param"]["contactDetail.phone"], True)
			else:
				person_info = Saas().get_id_phone(env=env, **kwg)
				if person_info is None:
					pytest.skip("未查询到用户数据,已忽略")
				params["request_param"]["IDCardInfo.cardNum"] = Common.info_crypt(env, person_info["id"], False)
				params["request_param"]["contactDetail.phone"] = Common.info_crypt(env, person_info["phone"], False)
				kwg["id"] = person_info["id"]
				kwg["phone"] = person_info["phone"]

		if "legal" in need_info:
			if use_source == "是":
				kwg["legal_id"] = Common.info_crypt(env, params["request_param"]["WorkCompanyLegalPerson.cardNum"],
													True)
				kwg["legal_phone"] = Common.info_crypt(env, params["request_param"]["WorkCompanyLegalPerson.mobile"],
													   True)
				kwg["work_company_id"] = params["request_param"]["WorkCompanyLegalPerson.workCompanyId"]
			else:
				legal_info = Saas().get_legal_info(env=env, **kwg)
				if legal_info is None:
					pytest.skip("未查询到用户数据,已忽略")
				params["request_param"]["WorkCompanyLegalPerson.cardNum"] = Common.info_crypt(env,
																							  legal_info["legal_id"],
																							  False)
				params["request_param"]["WorkCompanyLegalPerson.mobile"] = Common.info_crypt(env,
																							 legal_info["legal_phone"],
																							 False)
				params["request_param"]["WorkCompanyLegalPerson.workCompanyId"] = legal_info["work_company_id"]
				kwg["legal_id"] = legal_info["legal_id"]
				kwg["legal_phone"] = legal_info["legal_phone"]
				kwg["work_company_id"] = legal_info["work_company_id"]

		if "enterprise" in need_info:
			enterprise = Saas().get_enterprise_certificate_num(env=env, **kwg)
			if enterprise is None:
				pytest.skip("未查询到用户数据,已忽略")
			params["request_param"]["yyd_businessInfo.enterpriseCertificateNum"] = str(
				enterprise["enterprise"]).replace("'", "").replace('"', "")
			kwg["enterprise"] = enterprise["enterprise"]

		if "vin" in need_info:
			vin = Saas().get_id_vin(env=env, **kwg)
			if vin is None:
				pytest.skip("未查询到用户数据,已忽略")
			params["request_param"]["otherApplyInfo.vin"] = vin["vin"]
			kwg["vin"] = vin["vin"]

		if "scene" in need_info:
			scene = Saas().get_scene_holder_id(env=env, **kwg)
			if scene is None:
				pytest.skip("未查询到用户数据,已忽略")
			params["request_param"]["sceneHolderInfo.sceneHolderId"] = scene["scene_id"]
			kwg["scene_holder_id"] = scene["scene_id"]

		# 获取客群参数
		user_group = params["request_param"].get("userGroupProps", None)

		if user_group is not None and len(user_group) > 0:
			for x in user_group:
				if x["key"] == "personalInfo.applicantClinicRelationship":
					kwg["relation_ship"] = x["value"]
				elif x["key"] == "personalInfo.isDoctor":
					kwg["is_doctor"] = x["value"]
				elif x["key"] == "_projectDetail.applyTerm":
					kwg["term"] = x["value"]
				elif x["key"] == "projectExtraInfo.vehicleEnergyType":
					kwg["vehicle_energy_type"] = x["value"]
				else:
					raise KeyError(f"发现新的客群参数:{x['key']}")

		self.log.info(f"组装完成后的参数为:\n{json.dumps(kwg, ensure_ascii=False)}")

		# 请求准实时指标接口
		res = Common.response(env, params)
		assert_that(res["resultCode"], "校验准实时指标接口返回结果").is_equal_to(2000)
		content = dict(res["content"])

		# 获取package_id对应的item
		index_config = SDI().get_items(env=env, **kwg)
		assert_that(len(content), "校验接口返回结果与数据库中配置的指标数量是否一致").is_equal_to(len(index_config))

		with soft_assertions():
			for c in content.keys():
				assert_that(c, f"校验指标:{c}是否在配置中").is_in(*[p["title"] for p in index_config])

		# 校验接口的值是否正确
		with soft_assertions():
			for key, value in content.items():
				data_result = getattr(Saas(), key)(env=env, **kwg)
				assert_that(value, f"校验指标:{key}是否与业务库中计算结果相等").is_equal_to(data_result)


if __name__ == '__main__':
	pytest.main()
