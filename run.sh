printf "传入的参数为: $*\n"

res=$(python3 run.py $*)

#oldIFS=$IFS
#
#IFS=" "
#
#new_argv_arr=($*)
#
#res=""
#
#echo "new_argv_arr: ${new_argv_arr[@]:1}\n"
#
#for v in ${new_argv_arr[@]:1}:
#do
#  IFS="="
#  new_v=($v)
#  if [ ${new_v[1]} != ":" ];then
#    res+="-e $v "
#  fi
#done
#
#IFS=$OLD_IFS

printf "处理后的参数为:$res\n"

container_name=$1

printf "容器名称为:${container_name}\n"

cloudloan_start=$(docker ps -f name=${container_name} |awk "NR!=1 {print $NF}")
cloudloan_no_start=$(docker ps -a -f name=${container_name} |awk "NR!=1 {print $NF}")
if [ ${#cloudloan_start} -ge 1 ]; then
    printf "发现启动中的${container_name}容器 进行停止与删除操作 \n"
    docker stop ${container_name}
    docker rm ${container_name}
    printf "容器删除完成 \n"
elif [ ${#cloudloan_no_start} -ge 1 ]; then
    printf "发现未启动的$1容器 进行删除操作 \n"
    docker rm ${container_name}
else
  printf "未发现启动中与未启动的${container_name}容器 \n"
fi
printf "环境初始化完成 开始打包镜像 \n"
docker build -t ${container_name} .
printf "镜像打包完成 启动容器 \n"
printf "镜像运行命令: docker run $res -i -v /etc/localtime:/etc/localtime --name ${container_name} ${container_name}\n"
docker run $res  -i -v /etc/localtime:/etc/localtime  --name ${container_name} ${container_name}
printf "容器启动成功 脚本执行完成 将docker内生成的allure报告转移到外部文件夹\n"
container_id=$(docker ps -a -f name=${container_name} |awk 'NR!=1 {print $1}')
docker cp ${container_id}:./results .
#printf "容器启动成功 脚本执行完成 将docker内生成的日志文件转移到外部文件夹\n"
#docker cp ${container_id}:./sdi/logs .
printf "删除无效镜像并停止容器\n"
docker rmi -f  `docker images | grep '<none>' | awk '{print $3}'`
printf "镜像删除完成 \n"
docker stop ${container_name}
workspace=$PWD
printf "工作目录为:${workspace}\n"
chmod 777 ${workspace}
chmod 777 "${workspace}/results"