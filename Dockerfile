FROM python:3.10-slim

COPY . ./sdi

RUN pip install -r ./sdi/requirements.txt -i https://mirrors.aliyun.com/pypi/simple/

ENV env=qa
ENV casepath=""

CMD pytest "/sdi/$casepath" "--env=$env"